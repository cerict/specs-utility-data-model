.. contents:: Table of Contents

========
Overview
========

The SPECS framework consists of a set of independent components which need a common data model so they can interact with each other. The SPECS Data Model is a Java library that contains common Java classes shared among SPECS components. The interaction among SPECS components goes through REST APIs. In this interaction the Java objects are serialized to JSON or XML and deserialized back to the same Java class on the other side.

=====
Usage
=====

To include the SPECS Data Model in your Maven-based project, add the following dependency to the project's pom.xml file:

.. code-block:: xml

 <dependency>
    <groupId>eu.specs-project.utility</groupId>
    <artifactId>data-model</artifactId>
    <version>0.1-SNAPSHOT</version>
 </dependency>

In non-Maven projects, download the SPECS Data Model library from the SPECS Maven repository::

 https://nexus.services.ieat.ro/nexus/content/repositories/specs-snapshots/eu/specs-project/utility/data-model/

Add the downloaded jar file to the project's classpath.

======
Notice
======
