package eu.specs.datamodel.enforcement;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class NotificationTest {

    @Test
    public void testDeserializationFromJson() throws IOException {
        ObjectMapper mapper = new ObjectMapper();

        Notification notification = mapper.readValue(
                this.getClass().getResource("/notification.json"),
                Notification.class);

        assertEquals(notification.getComponent(), "specs_enforcement_sva");
        assertEquals(notification.getSlaId(), "56f2a6efe4b0d66a5b26f65e_offer4");
        assertEquals(notification.getMeasurementId(), "list_availability_sva_msr7");
        assertEquals(notification.getValue(), "true");
    }
}