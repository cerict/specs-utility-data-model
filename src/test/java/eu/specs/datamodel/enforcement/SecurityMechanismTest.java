package eu.specs.datamodel.enforcement;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class SecurityMechanismTest {

    @Test
    public void testDeserializationFromJson() throws IOException {
        ObjectMapper mapper = new ObjectMapper();

        SecurityMechanism mechanismSva = mapper.readValue(
                this.getClass().getResource("/mechanism-sva.json"),
                SecurityMechanism.class);

        assertEquals(mechanismSva.getName(), "sva");
        assertEquals(mechanismSva.getMetadata().getComponents().get(0).getName(), "sva_dashboard");
        assertEquals(mechanismSva.getChefRecipes().get(0).getName(), "sva_r1");
    }
}