package eu.specs.datamodel.enforcement;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class ImplementationPlanTest {

    @Test
    public void testDeserializationFromJson() throws IOException {
        ObjectMapper mapper = new ObjectMapper();

        ImplementationPlanOneProvider implPlan = mapper.readValue(
                this.getClass().getResource("/impl-plan.json"),
                ImplementationPlanOneProvider.class);

        assertEquals(implPlan.getSlaId(), "WebPool_SVA_1");
        assertEquals(implPlan.getIaas().getProvider(), "cloud.info.uvt.ro");
        assertEquals(implPlan.getPools().get(0).getVms().get(0).getComponents().get(0).getCookbook(), "WebPool");
    }
}