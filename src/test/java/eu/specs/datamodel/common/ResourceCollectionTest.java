package eu.specs.datamodel.common;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class ResourceCollectionTest {

    @Test
    public void testDeserializationFromJson() throws IOException {
        ObjectMapper mapper = new ObjectMapper();

        ResourceCollection resourceCollection = mapper.readValue(
                this.getClass().getResource("/resource-collection.json"),
                ResourceCollection.class);

        assertEquals(resourceCollection.getResource(), "annotation");
        assertEquals(resourceCollection.getTotal(), 100);
        assertEquals(resourceCollection.getMembers(), 10);
        assertEquals(resourceCollection.getItemList().get(0).getId(), "12");
        assertEquals(resourceCollection.getItemList().get(0).getItem(), "http://localhost/slas/67/annotations/12");
    }
}