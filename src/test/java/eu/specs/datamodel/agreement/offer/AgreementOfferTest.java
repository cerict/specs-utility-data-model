package eu.specs.datamodel.agreement.offer;

import eu.specs.datamodel.agreement.terms.GuaranteeTerm;
import eu.specs.datamodel.agreement.terms.ServiceDescriptionTerm;
import eu.specs.datamodel.common.TermExtractor;
import eu.specs.datamodel.control_frameworks.AbstractSecurityControl;
import eu.specs.datamodel.control_frameworks.ccm.CCMSecurityControl;
import eu.specs.datamodel.control_frameworks.nist.NISTSecurityControl;
import eu.specs.datamodel.sla.sdt.CapabilityType;
import eu.specs.datamodel.sla.sdt.SLOType;
import eu.specs.datamodel.sla.sdt.ServiceDescriptionType;
import org.junit.Test;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class AgreementOfferTest {




    @Test
    public void testNISTSerialization() throws Exception {
        JAXBContext jaxbContext = JAXBContext.newInstance(AgreementOffer.class);
        Unmarshaller u = jaxbContext.createUnmarshaller();
        //String xml = readStream(this.getClass().getResourceAsStream("/SLAtemplate_NIST_new.xml"));
        AgreementOffer agreementOffer = (AgreementOffer) u.unmarshal(
                this.getClass().getResourceAsStream("/SLAtemplate_NIST_new.xml")
        );

        assertEquals("WebPool_SVA", agreementOffer.getName());

        Marshaller marshaller = jaxbContext.createMarshaller();
        StringWriter stringWriter = new StringWriter();
        marshaller.marshal(agreementOffer, stringWriter);
        String agreementOfferXml = stringWriter.toString();

        AgreementOffer agreementOffer1 = (AgreementOffer) u.unmarshal(
                new StringReader(agreementOfferXml));

        assertEquals(agreementOffer1.getName(), agreementOffer.getName());

        testIfElementsNotNull(agreementOffer, "nist");
    }

    @Test
    public void testCCMSerialization() throws Exception {
        JAXBContext jaxbContext = JAXBContext.newInstance(AgreementOffer.class);
        Unmarshaller u = jaxbContext.createUnmarshaller();
        String xml = readStream(this.getClass().getResourceAsStream("/SLAtemplate_CCM.xml"));
        AgreementOffer agreementOffer = (AgreementOffer) u.unmarshal(
                this.getClass().getResourceAsStream("/SLAtemplate_CCM_new.xml")
        );

        assertEquals("Y2-SPECS-APP", agreementOffer.getName());

        Marshaller marshaller = jaxbContext.createMarshaller();
        StringWriter stringWriter = new StringWriter();
        marshaller.marshal(agreementOffer, stringWriter);
        String agreementOfferXml = stringWriter.toString();

//        assertEquals(xml, agreementOfferXml);

        AgreementOffer agreementOffer1 = (AgreementOffer) u.unmarshal(
                new StringReader(agreementOfferXml));

        assertEquals(agreementOffer1.getName(), agreementOffer.getName());
        testIfElementsNotNull(agreementOffer, "ccm");

    }

    public void testIfElementsNotNull(AgreementOffer agreementOffer, String controlType){
        ServiceDescriptionTerm sdt = (ServiceDescriptionTerm) TermExtractor.
                getTermsOfType(agreementOffer.getTerms().getAll().getAll(),ServiceDescriptionTerm.class).get(0);

        GuaranteeTerm guaranteeTerm = (GuaranteeTerm) TermExtractor.getTermsOfType(
                agreementOffer.getTerms().getAll().getAll(), GuaranteeTerm.class).get(0);

        assertNotNull(sdt.getServiceDescription());
        assertNotNull(sdt.getServiceDescription().getServiceResources());
        List<ServiceDescriptionType.ServiceResources> resources = sdt.getServiceDescription().getServiceResources();

        for(ServiceDescriptionType.ServiceResources res : resources){
            assertNotNull(res.getResourcesProvider());
            for(ServiceDescriptionType.ServiceResources.ResourcesProvider rp : res.getResourcesProvider()){
                assertNotNull(rp.getName());
                assertNotNull(rp.getDescription());
                assertNotNull(rp.getId());
                assertNotNull(rp.getMaxAllowedVMs());
                assertNotNull(rp.getLabel());
                assertNotNull(rp.getZone());
                assertNotNull(rp.getVM());
                for(ServiceDescriptionType.ServiceResources.ResourcesProvider.VM vm : rp.getVM()){
                    assertNotNull(vm);
                    assertNotNull(vm.getAppliance());
                    assertNotNull(vm.getDescr());
                    assertNotNull(vm.getHardware());
                }
            }
        }

        assertNotNull(guaranteeTerm);
        assertNotNull(guaranteeTerm.getServiceLevelObjective());
        assertNotNull(guaranteeTerm.getServiceLevelObjective().getCustomServiceLevel());
        assertNotNull(guaranteeTerm.getServiceLevelObjective().getCustomServiceLevel().getObjectiveList());
        List<SLOType> slos = guaranteeTerm.getServiceLevelObjective().getCustomServiceLevel().getObjectiveList().getSLO();

        assertNotNull(slos);
        for(SLOType slo : slos){
            assertNotNull(slo.getImportanceWeight());
            assertNotNull(slo.getSLOexpression());
            if(slo.getSLOexpression().getOneOpExpression() != null){
                assertNotNull(slo.getSLOexpression().getOneOpExpression().getOperand());
                assertNotNull(slo.getSLOexpression().getOneOpExpression().getOperator());
            }
            if(slo.getSLOexpression().getTwoOpExpression() != null){
                assertNotNull(slo.getSLOexpression().getTwoOpExpression().getOperator());
                assertNotNull(slo.getSLOexpression().getTwoOpExpression().getOperand1());
                assertNotNull(slo.getSLOexpression().getTwoOpExpression().getOperand2());
            }
            assertNotNull(slo.getMetricREF());
            assertNotNull(slo.getSLOID());

        }

        assertNotNull(sdt);
        assertNotNull(sdt.getServiceDescription());
        assertNotNull(sdt.getServiceDescription().getCapabilities());
        assertNotNull(sdt.getServiceDescription().getCapabilities().getCapability());
        List<CapabilityType> capabilityTypeList = sdt.getServiceDescription().getCapabilities().getCapability();
        for(CapabilityType capability : capabilityTypeList){
            List<AbstractSecurityControl> securityControls = capability.getControlFramework().getSecurityControl();
            assertNotNull(securityControls);
            for(Object asc : securityControls){
                if("nist".equals(controlType)) {
                    NISTSecurityControl nistControl = (NISTSecurityControl) asc;
                    assertNotNull(nistControl.getId());
                    assertNotNull(nistControl.getImportanceWeight());
                    assertNotNull(nistControl.getControlDescription());
                    assertNotNull(nistControl.getControlFamily());
                    assertNotNull(nistControl.getControlEnhancement());
                    assertNotNull(nistControl.getName());
                }else{
                    CCMSecurityControl ccmControl = (CCMSecurityControl) asc;
                    assertNotNull(ccmControl.getId());
                    assertNotNull(ccmControl.getImportanceWeight());
                    assertNotNull(ccmControl.getControlDescription());
                    assertNotNull(ccmControl.getControlDomain());
                    assertNotNull(ccmControl.getName());
                }
            }
        }
    }

    public static String readStream(InputStream is) {
        StringBuilder sb = new StringBuilder();
        try {
            Reader r = new InputStreamReader(is, "UTF-8");
            int c = 0;
            while ((c = r.read()) != -1) {
                sb.append((char) c);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return sb.toString();
    }

}
