package eu.specs.datamodel.agreement.offer;


import eu.specs.datamodel.agreement.Context;
import eu.specs.datamodel.agreement.terms.Terms;
import lombok.Data;
import org.springframework.data.annotation.Id;

import javax.xml.bind.annotation.*;
import java.io.Serializable;

/**
 * Created by adispataru on 4/24/15.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {
        "name", "context", "terms"
})
@Data
@XmlRootElement(name = "AgreementOffer", namespace = "http://schemas.ggf.org/graap/2007/03/ws-agreement")
public class AgreementOffer implements Serializable{

    private static final long serialVersionUID = -736691391213782012L;
    @XmlElement(name = "Name", namespace = "http://schemas.ggf.org/graap/2007/03/ws-agreement")
    @Id
    private String name;

    @XmlElement(name = "Context", namespace = "http://schemas.ggf.org/graap/2007/03/ws-agreement")
    private Context context;

    @XmlElement(name = "Terms", namespace = "http://schemas.ggf.org/graap/2007/03/ws-agreement")
    private Terms terms;

    public AgreementOffer(){

    }

    public AgreementOffer(AgreementOffer offer){
        this.name = offer.getName();
        this.context = offer.getContext();
        this.terms = offer.getTerms();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public Terms getTerms() {
        return terms;
    }

    public void setTerms(Terms terms) {
        this.terms = terms;
    }
}
