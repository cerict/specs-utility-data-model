package eu.specs.datamodel.credentials.broker;

public class CredentialExample {
	public String prova1;
	public String prova2;
	public String prova3;
	public String getProva1() {
		return prova1;
	}
	public void setProva1(String prova1) {
		this.prova1 = prova1;
	}
	public String getProva2() {
		return prova2;
	}
	public void setProva2(String prova2) {
		this.prova2 = prova2;
	}
	public String getProva3() {
		return prova3;
	}
	public void setProva3(String prova3) {
		this.prova3 = prova3;
	}
	
}
