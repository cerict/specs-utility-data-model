package eu.specs.datamodel.credentials.broker;

public class EucalyptusCredentials {

	private String chefUserPrivateKey;
	private String chefOrganizationPrivateKey;
	private String providerUserPrivateKey;
	private String providerUserName;
	private String providerUserPassword;
	
	public String getChefUserPrivateKey() {
		return chefUserPrivateKey;
	}
	public void setChefUserPrivateKey(String chefUserPrivateKey) {
		this.chefUserPrivateKey = chefUserPrivateKey;
	}
	public String getChefOrganizationPrivateKey() {
		return chefOrganizationPrivateKey;
	}
	public void setChefOrganizationPrivateKey(String chefOrganizationPrivateKey) {
		this.chefOrganizationPrivateKey = chefOrganizationPrivateKey;
	}
	public String getProviderUserPrivateKey() {
		return providerUserPrivateKey;
	}
	public void setProviderUserPrivateKey(String providerUserPrivateKey) {
		this.providerUserPrivateKey = providerUserPrivateKey;
	}
	public String getProviderUserName() {
		return providerUserName;
	}
	public void setProviderUserName(String providerUserName) {
		this.providerUserName = providerUserName;
	}
	public String getProviderUserPassword() {
		return providerUserPassword;
	}
	public void setProviderUserPassword(String providerUserPassword) {
		this.providerUserPassword = providerUserPassword;
	}

}
