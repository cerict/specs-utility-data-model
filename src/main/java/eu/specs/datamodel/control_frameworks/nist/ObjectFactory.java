
package eu.specs.datamodel.control_frameworks.nist;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the main.java.main.java.negotiation.eu.specs.negotiation.main.java.negotiation.eu.specs.main.java.negotiation.eu.specs.negotiation.control_frameworks package.
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SecurityControls_QNAME = new QName("http://www.specs-project.eu/resources/schemas/xml/control_frameworks/nist", "securityControl");
    private final static QName _Id_QNAME = new QName("http://www.specs-project.eu/resources/schemas/xml/control_frameworks/nist", "id");
    private final static QName _ControlFamily_QNAME = new QName("http://www.specs-project.eu/resources/schemas/xml/control_frameworks/nist", "control_family");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: main.java.main.java.negotiation.eu.specs.negotiation.main.java.negotiation.eu.specs.main.java.negotiation.eu.specs.negotiation.control_frameworks
     * 
     */
    public ObjectFactory() {
    }


    /**
     * Create an instance of {@link NISTSecurityControl }
     * 
     */
    public NISTSecurityControl createNISTsecurityControl() {
        return new NISTSecurityControl();
    }

    @XmlElementDecl(namespace = "http://www.specs-project.eu/resources/schemas/xml/control_frameworks/nist", name = "securityControl")
    public JAXBElement<String> createSecurityControl(String value) {
        return new JAXBElement<String>(_SecurityControls_QNAME, String.class, null, value);
    }

    @XmlElementDecl(namespace = "http://www.specs-project.eu/resources/schemas/xml/control_frameworks/nist", name = "id")
    public JAXBElement<String> createId(String value) {
        return new JAXBElement<String>(_Id_QNAME, String.class, null, value);
    }

    @XmlElementDecl(namespace = "http://www.specs-project.eu/resources/schemas/xml/control_frameworks/nist", name = "control_family")
    public JAXBElement<String> createcontrolFamily(String value) {
        return new JAXBElement<String>(_ControlFamily_QNAME, String.class, null, value);
    }


}
