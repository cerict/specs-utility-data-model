package eu.specs.datamodel.broker;

import java.util.ArrayList;
import java.util.List;

public class NodesInfo{
    private String rootPassword;
    private String privateKey;
    private String publicKey;
    private List<ClusterNode> nodes;
    
    
    public NodesInfo() {
        this.nodes = new ArrayList<ClusterNode>();
    }

    public String getRootPassword() {
        return rootPassword;
    }

    public void setRootPassword(String rootPassword) {
        this.rootPassword = rootPassword;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    public List<ClusterNode> getNodes() {
        return nodes;
    }

    public void setNodes(List<ClusterNode> nodes) {
        this.nodes = nodes;
    }
    
    public void addNode(ClusterNode node){
        this.nodes.add(node);
    }
    
    public void addNodes(List<ClusterNode> nodes){
    	for(ClusterNode node : nodes){
    		this.nodes.add(node);
    	}
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }
}
