package eu.specs.datamodel.broker;

import java.util.HashMap;
import java.util.Map;

import eu.specs.datamodel.enforcement.ImplementationPlanTwoProviders.Csp;


public class ProviderCredentialsManager {
	
	private Map <Csp, ProviderCredential> map = new HashMap<Csp, ProviderCredential>();


	public void add(Csp csp, ProviderCredential cred){
			map.put(csp, cred);	
	}
	
	 
	public ProviderCredential getCredentials(Csp csp){
		return map.get(csp);
	}


	public Map<Csp, ProviderCredential> getMap() {
		return map;
	}
	
	
	
}