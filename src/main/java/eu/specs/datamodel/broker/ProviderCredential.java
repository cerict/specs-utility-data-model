package eu.specs.datamodel.broker;

public class ProviderCredential {

	private String username, password, cloudAddress;

	public ProviderCredential(String username, String password, String cloudAddress) {
		super();
		this.username = username;
		this.password = password;
		this.cloudAddress=cloudAddress;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public String getCloudAddress() {
		return cloudAddress;
	}    

}