package eu.specs.datamodel.enforcement;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")


public class ImplementationPlanPaas extends ImplementationPlanTwoProviders{
	
	@JsonProperty("plan_id")
    private String id;

    @JsonProperty("sla_id")
    private String slaId;
    
	private Integer monitoring_core_port;
	private List<Measurement> measurements = new ArrayList<Measurement>();
	private String monitoring_core_ip;
	private String creation_time;
//	private List<Csps> csps = new ArrayList<Csps>();
	private List<Object> annotations = new ArrayList<Object>();
	private List<Slo> slos = new ArrayList<Slo>();

	public ImplementationPlanPaas(){
		
	}
	
	public Integer getMonitoring_core_port (){
		return monitoring_core_port;
	}

	public void setMonitoring_core_port (Integer monitoring_core_port){
		this.monitoring_core_port = monitoring_core_port;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSlaId() {
		return slaId;
	}

	public void setSlaId(String slaId) {
		this.slaId = slaId;
	}

	public String getMonitoring_core_ip (){
		return monitoring_core_ip;
	}

	public void setMonitoring_core_ip (String monitoring_core_ip){
		this.monitoring_core_ip = monitoring_core_ip;
	}

	public String getCreation_time (){
		return creation_time;
	}

	public void setCreation_time (String creation_time){
		this.creation_time = creation_time;
	}

	public List<Measurement> getMeasurements() {
		return measurements;
	}

	public void setMeasurements(List<Measurement> measurements) {
		this.measurements = measurements;
	}
	
	public void addMeasurement(Measurement measurement) {
		this.measurements.add(measurement);
	}

//	public List<Csps> getCsps() {
//		return csps;
//	}
//
//	public void setCsps(List<Csps> csps) {
//		this.csps = csps;
//	}

	public List<Object> getAnnotations() {
		return annotations;
	}

	public void setAnnotations(List<Object> annotations) {
		this.annotations = annotations;
	}

	public List<Slo> getSlos() {
		return slos;
	}

	public void setSlos(List<Slo> slos) {
		this.slos = slos;
	}

//	@Override
//	public String toString(){
//		return "ClassPojo [monitoring_core_port = "+monitoring_core_port+", measurements = "+measurements+", sla_id = "+slaId+", monitoring_core_ip = "+monitoring_core_ip+", creation_time = "+creation_time+", csps = "+csps+", annotations = "+annotations+", slos = "+slos+"]";
//	}

/*
	public static class Csps{
		private Paas paas;
		private List<Pools> pools = new ArrayList<Pools>();
		private Iaas iaas;

		public Paas getPaas (){
			return paas;
		}

		public void setPaas (Paas paas){
			this.paas = paas;
		}
		
		public List<Pools> getPools() {
			return pools;
		}

		public void setPools(List<Pools> pools) {
			this.pools = pools;
		}

		public Iaas getIaas (){
			return iaas;
		}

		public void setIaas (Iaas iaas){
			this.iaas = iaas;
		}

		@Override
		public String toString(){
			return "ClassPojo [paas = "+paas+", pools = "+pools+", iaas = "+iaas+"]";
		}
	}

	*/
	
	/*
	public static class Pools{
		private Object nodes_access_credentials_reference;
		private List<Vms> vms = new ArrayList<Vms>();
		private String pool_seq_num;
		private Object pool_name;
		private String network;

		public Object getNodes_access_credentials_reference (){
			return nodes_access_credentials_reference;
		}

		public void setNodes_access_credentials_reference (Object nodes_access_credentials_reference){
			this.nodes_access_credentials_reference = nodes_access_credentials_reference;
		}
		
		public List<Vms> getVms() {
			return vms;
		}

		public void setVms(List<Vms> vms) {
			this.vms = vms;
		}

		public String getPool_seq_num (){
			return pool_seq_num;
		}

		public void setPool_seq_num (String pool_seq_num){
			this.pool_seq_num = pool_seq_num;
		}

		public Object getPool_name (){
			return pool_name;
		}

		public void setPool_name (Object pool_name){
			this.pool_name = pool_name;
		}

		public String getNetwork (){
			return network;
		}

		public void setNetwork (String network){
			this.network = network;
		}

		@Override
		public String toString(){
			return "ClassPojo [nodes_access_credentials_reference = "+nodes_access_credentials_reference+", vms = "+vms+", pool_seq_num = "+pool_seq_num+", pool_name = "+pool_name+", network = "+network+"]";
		}
	}

	*/
	
	/*
	public static class Vms implements Serializable{


		private static final long serialVersionUID = 4067646998961776401L;
		private String hardware;
		private String public_ip;
		private String vm_seq_num;
		private List<Component> components = new ArrayList<Component>();
		private String appliance;

		public String getHardware (){
			return hardware;
		}

		public void setHardware (String hardware){
			this.hardware = hardware;
		}

		public String getPublic_ip (){
			return public_ip;
		}

		public void setPublic_ip (String public_ip){
			this.public_ip = public_ip;
		}

		public String getVm_seq_num (){
			return vm_seq_num;
		}

		public void setVm_seq_num (String vm_seq_num){
			this.vm_seq_num = vm_seq_num;
		}
		
		public List<Component> getComponents() {
			return components;
		}

		public void setComponents(List<Component> components) {
			this.components = components;
		}

		public static long getSerialversionuid() {
			return serialVersionUID;
		}

		public String getAppliance (){
			return appliance;
		}

		public void setAppliance (String appliance){
			this.appliance = appliance;
		}

		@Override
		public String toString(){
			return "ClassPojo [hardware = "+hardware+", public_ip = "+public_ip+", vm_seq_num = "+vm_seq_num+", components = "+components+", appliance = "+appliance+"]";
		}
	}

	*/
	
	/*
	public static class Components{
		private String recipe;
		@JsonProperty("component_id")
        private String componentId;
		private Firewall firewall;
		private String cookbook;
		private String acquire_public_ip;
		@JsonProperty("private_ips")
		private List<String> privateIps = new ArrayList<String>();
		private String implementation_step;

		public String getRecipe (){
			return recipe;
		}

		public void setRecipe (String recipe){
			this.recipe = recipe;
		}

		
		
		public String getComponentId() {
			return componentId;
		}

		public void setComponentId(String componentId) {
			this.componentId = componentId;
		}

		public Firewall getFirewall (){
			return firewall;
		}

		public void setFirewall (Firewall firewall){
			this.firewall = firewall;
		}

		public String getCookbook (){
			return cookbook;
		}

		public void setCookbook (String cookbook){
			this.cookbook = cookbook;
		}

		public String getAcquire_public_ip (){
			return acquire_public_ip;
		}

		public void setAcquire_public_ip (String acquire_public_ip){
			this.acquire_public_ip = acquire_public_ip;
		}

		public List<String> getPrivateIps() {
			return privateIps;
		}

		public void setPrivateIps(List<String> privateIps) {
			this.privateIps = privateIps;
		}

		public String getImplementation_step (){
			return implementation_step;
		}

		public void setImplementation_step (String implementation_step){
			this.implementation_step = implementation_step;
		}

		@Override
		public String toString(){
			return "ClassPojo [recipe = "+recipe+", component_id = "+componentId+", firewall = "+firewall+", cookbook = "+cookbook+", acquire_public_ip = "+acquire_public_ip+", private_ips = "+privateIps+", implementation_step = "+implementation_step+"]";
		}
	}

	
	*/

	/*
	public static class Proto{
		@JsonProperty("port_list")
        private List<String> port_list;
		private String type;
		
		@JsonProperty("port_list")
		public List<String> getPort_list() {
			return port_list;
		}
		@JsonProperty("port_list")
		public void setPort_list(List<String> port_list) {
			this.port_list = port_list;
		}

		public String getType (){
			return type;
		}

		public void setType (String type){
			this.type = type;
		}

		@Override
		public String toString(){
			return "ClassPojo [port_list = "+port_list+", type = "+type+"]";
		}
	}
*/

	/*
	public static class Firewall{
		private Incoming incoming;
		private Outcoming outcoming;

		public Incoming getIncoming (){
			return incoming;
		}

		public void setIncoming (Incoming incoming){
			this.incoming = incoming;
		}

		public Outcoming getOutcoming (){
			return outcoming;
		}

		public void setOutcoming (Outcoming outcoming){
			this.outcoming = outcoming;
		}

		@Override
		public String toString(){
			return "ClassPojo [incoming = "+incoming+", outcoming = "+outcoming+"]";
		}
	}
	*/
/*
	public static class Incoming{
		@JsonProperty("source_nodes")
		private List<Object> sourceNodes = new ArrayList<Object>();
		@JsonProperty("interface")
		private String _interface;
		@JsonProperty("source_ips")
        private List<Object> sourceIps = new ArrayList<Object>();
        private List<Proto> proto = new ArrayList<Proto>();

		public String get_interface() {
			return _interface;
		}

		public void set_interface(String _interface) {
			this._interface = _interface;
		}
		@JsonProperty("source_nodes")
		public List<Object> getSourceNodes() {
			return sourceNodes;
		}
		@JsonProperty("source_nodes")
		public void setSourceNodes(List<Object> sourceNodes) {
			this.sourceNodes = sourceNodes;
		}

		public List<Object> getSourceIps() {
			return sourceIps;
		}

		public void setSourceIps(List<Object> sourceIps) {
			this.sourceIps = sourceIps;
		}
		
		public List<Proto> getProto() {
			return proto;
		}
		
		public void setProto(List<Proto> proto) {
			this.proto = proto;
		}

		@Override
		public String toString(){
			return "ClassPojo [source_nodes = "+sourceNodes+", interface = "+_interface+", source_ips = "+sourceIps+", proto = "+proto+"]";
		}
	}
*/
	
	/*
	public static class Component implements Serializable{

	    private static final long serialVersionUID = 2100338615434002629L;
	        
	    @JsonProperty("component_id")
	    private String componentId;
	    
	    @JsonProperty("component_type")
	    private String type;
	    @JsonProperty("recipe")
	    private String recipe;
	    @JsonProperty("cookbook")
	    private String cookbook;
	    @JsonProperty("implementation_step")
	    private int implementationStep;
	    @JsonProperty("pool_seq_num")
	    private int poolSeqNum;
	    @JsonProperty("pool_id")
	    private String poolId;
	    @JsonProperty("vm_requirement")
	    private VmRequirement vmRequirement;
	    @JsonProperty("acquire_public_ip")
        private Boolean acquirePublicIp;
	    @JsonProperty("firewall")
        private Firewall firewall;
	    @JsonProperty("private_ips")
        private List<String> privateIps = new ArrayList<String>();

	    
	    public Component(){
	    	
	    }
	    
	    public String getType() {
	        return type;
	    }

	    public void setType(String type) {
	        this.type = type;
	    }

	    public Firewall getFirewall() {
			return firewall;
		}

		public void setFirewall(Firewall firewall) {
			this.firewall = firewall;
		}

		public List<String> getPrivateIps() {
			return privateIps;
		}

		public void setPrivateIps(List<String> privateIps) {
			this.privateIps = privateIps;
		}

		public static long getSerialversionuid() {
			return serialVersionUID;
		}

		public String getComponentId() {
			return componentId;
		}

		public void setComponentId(String componentId) {
			this.componentId = componentId;
		}

		public Boolean getAcquirePublicIp() {
			return acquirePublicIp;
		}

		public void setAcquirePublicIp(Boolean acquirePublicIp) {
			this.acquirePublicIp = acquirePublicIp;
		}

		public String getRecipe() {
	        return recipe;
	    }

	    public void setRecipe(String recipe) {
	        this.recipe = recipe;
	    }

	    public String getCookbook() {
	        return cookbook;
	    }

	    public void setCookbook(String cookbook) {
	        this.cookbook = cookbook;
	    }

	    public int getImplementationStep() {
	        return implementationStep;
	    }

	    public void setImplementationStep(int implementationStep) {
	        this.implementationStep = implementationStep;
	    }

	    public int getPoolSeqNum() {
	        return poolSeqNum;
	    }

	    public void setPoolSeqNum(int poolSeqNum) {
	        this.poolSeqNum = poolSeqNum;
	    }

	    public String getPoolId() {
	        return poolId;
	    }

	    public void setPoolId(String poolId) {
	        this.poolId = poolId;
	    }

	    public VmRequirement getVmRequirement() {
	        return vmRequirement;
	    }

	    public void setVmRequirement(VmRequirement vmRequirement) {
	        this.vmRequirement = vmRequirement;
	    }

	    public class VmRequirement implements Serializable{

	        private static final long serialVersionUID = 4030075570750680452L;
	        @JsonProperty("hardware")
	        private String hardware;
	        @JsonProperty("usage")
	        private String usage;
	        @JsonProperty("acquire_public_ip")
	        private Boolean acquirePublicIp;
	        @JsonProperty("private_ips_count")
	        private int privateIpsCount;
	        @JsonProperty("firewall")
	        private Firewall firewall;

	        public String getHardware() {
	            return hardware;
	        }

	        public void setHardware(String hardware) {
	            this.hardware = hardware;
	        }

	        public String getUsage() {
	            return usage;
	        }

	        public void setUsage(String usage) {
	            this.usage = usage;
	        }

	        public boolean isAcquirePublicIp() {
	            return acquirePublicIp;
	        }

	        public void setAcquirePublicIp(boolean acquirePublicIp) {
	            this.acquirePublicIp = acquirePublicIp;
	        }

	        public int getPrivateIpsCount() {
	            return privateIpsCount;
	        }

	        public void setPrivateIpsCount(int privateIpsCount) {
	            this.privateIpsCount = privateIpsCount;
	        }

	        public Firewall getFirewall() {
	            return firewall;
	        }

	        public void setFirewall(Firewall firewall) {
	            this.firewall = firewall;
	        }
	    }

	    public class Firewall implements Serializable{

			private static final long serialVersionUID = 7194492386962002642L;
			@JsonProperty("incoming")
	        private Incoming incoming;
	        @JsonProperty("outcoming")
	        private Outcoming outcoming;

	        public Incoming getIncoming() {
	            return incoming;
	        }

	        public void setIncoming(Incoming incoming) {
	            this.incoming = incoming;
	        }

	        public Outcoming getOutcoming() {
	            return outcoming;
	        }

	        public void setOutcoming(Outcoming outcoming) {
	            this.outcoming = outcoming;
	        }

	        public class Incoming implements Serializable{
	            private static final long serialVersionUID = 4032481177412064603L;
	            @JsonProperty("source_ips")
	            private List<String> sourceIps;
	            @JsonProperty("source_nodes")
	            private List<String> sourceNodes;
	            @JsonProperty("interface")
	            private String targetInterface;
	            
	            @JsonProperty("proto")
	            private List<Proto> proto;
	            
	            @JsonProperty("port_list")
	            private List<String> portList;
				public List<String> getSourceIps() {
					return sourceIps;
				}
				public void setSourceIps(List<String> sourceIps) {
					this.sourceIps = sourceIps;
				}
				public List<String> getSourceNodes() {
					return sourceNodes;
				}
				public void setSourceNodes(List<String> sourceNodes) {
					this.sourceNodes = sourceNodes;
				}
				public String getTargetInterface() {
					return targetInterface;
				}
				public void setTargetInterface(String targetInterface) {
					this.targetInterface = targetInterface;
				}
				public List<Proto> getProto() {
					return proto;
				}
				public void setProto(List<Proto> proto) {
					this.proto = proto;
				}
				public List<String> getPortList() {
					return portList;
				}
				public void setPortList(List<String> portList) {
					this.portList = portList;
				}
	            
	            
	        }

	        public class Outcoming implements Serializable{
	            private static final long serialVersionUID = -8199070091162031438L;
	            @JsonProperty("destination_ips")
	            private List<String> destinationIps;
	            @JsonProperty("destination_nodes")
	            private List<String> destinationNodes;
	            @JsonProperty("interface")
	            private String targetInterface;
	            @JsonProperty("proto")
	            private List<Proto> proto;
	            @JsonProperty("port_list")
	            private List<String> portList;
				public List<String> getDestinationIps() {
					return destinationIps;
				}
				public void setDestinationIps(List<String> destinationIps) {
					this.destinationIps = destinationIps;
				}
				public List<String> getDestinationNodes() {
					return destinationNodes;
				}
				public void setDestinationNodes(List<String> destinationNodes) {
					this.destinationNodes = destinationNodes;
				}
				public String getTargetInterface() {
					return targetInterface;
				}
				public void setTargetInterface(String targetInterface) {
					this.targetInterface = targetInterface;
				}
				public List<Proto> getProto() {
					return proto;
				}
				public void setProto(List<Proto> proto) {
					this.proto = proto;
				}
				public List<String> getPortList() {
					return portList;
				}
				public void setPortList(List<String> portList) {
					this.portList = portList;
				}	            
	        }
	    }
	}
	*/
	
	public static class Paas{
		@JsonProperty("components")
        private List<Component> components = new ArrayList<Component>();
		private String paasAgent;
		
		public List<Component> getComponents() {
			return components;
		}

		public void setComponents(List<Component> components) {
			this.components = components;
		}

		public String getPaasAgent (){
			return paasAgent;
		}

		public void setPaasAgent (String paasAgent){
			this.paasAgent = paasAgent;
		}

		@Override
		public String toString(){
			return "ClassPojo [components = "+components+", paasAgent = "+paasAgent+"]";
		}
	}

	/*
	public static class Outcoming{
		@JsonProperty("destination_ips")
        private List<Object> destinationIps = new ArrayList<Object>();
        @JsonProperty("destination_nodes")
        private List<Object> destinationNodes = new ArrayList<Object>();
		@JsonProperty("interface")
		private String _interface;
		private List<Proto> proto = new ArrayList<Proto>();

		
		
		public List<Object> getDestinationIps() {
			return destinationIps;
		}

		public void setDestinationIps(List<Object> destinationIps) {
			this.destinationIps = destinationIps;
		}

		public List<Object> getDestinationNodes() {
			return destinationNodes;
		}

		public void setDestinationNodes(List<Object> destinationNodes) {
			this.destinationNodes = destinationNodes;
		}

		public String get_interface() {
			return _interface;
		}

		public void set_interface(String _interface) {
			this._interface = _interface;
		}

		public List<Proto> getProto() {
			return proto;
		}

		public void setProto(List<Proto> proto) {
			this.proto = proto;
		}

		@Override
		public String toString(){
			return "ClassPojo [destination_ips = "+destinationIps+", destination_nodes = "+destinationNodes+", interface = "+_interface+", proto = "+proto+"]";
		}
	}
	*/

	/*
	public static class Iaas{	
		private String provider;
		private String user;
		private String network;
		private String zone;
		
		public Iaas (){
			
		}

		
		
		public String getProvider (){
			return provider;
		}

		public void setProvider (String provider){
			this.provider = provider;
		}

		public String getUser (){
			return user;
		}

		public void setUser (String user){
			this.user = user;
		}

		public String getNetwork (){
			return network;
		}

		public void setNetwork (String network){
			this.network = network;
		}

		public String getZone (){
			return zone;
		}

		public void setZone (String zone){
			this.zone = zone;
		}

		@Override
		public String toString(){
			return "ClassPojo [provider = "+provider+", user = "+user+", network = "+network+", zone = "+zone+"]";
		}
	}
	*/
}