package eu.specs.datamodel.enforcement;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

public class RemAction implements Serializable {

    private static final long serialVersionUID = 3082260310688515991L;
    @JsonProperty("name")
    private String name;
    @JsonProperty("action_description")
    private String description;
    @JsonProperty("recipes")
    private List<String> recipes;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getRecipes() {
        return recipes;
    }

    public void setRecipes(List<String> recipes) {
        this.recipes = recipes;
    }
}
