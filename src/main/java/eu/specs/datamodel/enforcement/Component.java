package eu.specs.datamodel.enforcement;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

public class Component implements Serializable{

    /**
     * 
     */
    private static final long serialVersionUID = 2100338615434002629L;
    
    @JsonProperty("component_name")
    private String name;
    @JsonProperty("component_type")
    private String type;
    @JsonProperty("recipe")
    private String recipe;
    @JsonProperty("cookbook")
    private String cookbook;
    @JsonProperty("implementation_step")
    private int implementationStep;
    @JsonProperty("pool_seq_num")
    private int poolSeqNum;
    @JsonProperty("pool_id")
    private String poolId;
    @JsonProperty("vm_requirement")
    private VmRequirement vmRequirement;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRecipe() {
        return recipe;
    }

    public void setRecipe(String recipe) {
        this.recipe = recipe;
    }

    public String getCookbook() {
        return cookbook;
    }

    public void setCookbook(String cookbook) {
        this.cookbook = cookbook;
    }

    public int getImplementationStep() {
        return implementationStep;
    }

    public void setImplementationStep(int implementationStep) {
        this.implementationStep = implementationStep;
    }

    public int getPoolSeqNum() {
        return poolSeqNum;
    }

    public void setPoolSeqNum(int poolSeqNum) {
        this.poolSeqNum = poolSeqNum;
    }

    public String getPoolId() {
        return poolId;
    }

    public void setPoolId(String poolId) {
        this.poolId = poolId;
    }

    public VmRequirement getVmRequirement() {
        return vmRequirement;
    }

    public void setVmRequirement(VmRequirement vmRequirement) {
        this.vmRequirement = vmRequirement;
    }

    public static class VmRequirement implements Serializable{
        /**
         * 
         */
        private static final long serialVersionUID = 4030075570750680452L;
        @JsonProperty("hardware")
        private String hardware;
        @JsonProperty("usage")
        private String usage;
        @JsonProperty("acquire_public_ip")
        private boolean acquirePublicIp;
        @JsonProperty("private_ips_count")
        private int privateIpsCount;
        @JsonProperty("firewall")
        private Firewall firewall;

        public String getHardware() {
            return hardware;
        }

        public void setHardware(String hardware) {
            this.hardware = hardware;
        }

        public String getUsage() {
            return usage;
        }

        public void setUsage(String usage) {
            this.usage = usage;
        }

        public boolean isAcquirePublicIp() {
            return acquirePublicIp;
        }

        public void setAcquirePublicIp(boolean acquirePublicIp) {
            this.acquirePublicIp = acquirePublicIp;
        }

        public int getPrivateIpsCount() {
            return privateIpsCount;
        }

        public void setPrivateIpsCount(int privateIpsCount) {
            this.privateIpsCount = privateIpsCount;
        }

        public Firewall getFirewall() {
            return firewall;
        }

        public void setFirewall(Firewall firewall) {
            this.firewall = firewall;
        }
    }

    public static class Firewall implements Serializable{

        @JsonProperty("incoming")
        private Incoming incoming;
        @JsonProperty("outcoming")
        private Outcoming outcoming;

        public Incoming getIncoming() {
            return incoming;
        }

        public void setIncoming(Incoming incoming) {
            this.incoming = incoming;
        }

        public Outcoming getOutcoming() {
            return outcoming;
        }

        public void setOutcoming(Outcoming outcoming) {
            this.outcoming = outcoming;
        }

        public static class Incoming implements Serializable{
            /**
             * 
             */
            private static final long serialVersionUID = 4032481177412064603L;
            @JsonProperty("source_ips")
            private List<String> sourceIps;
            @JsonProperty("source_nodes")
            private List<String> sourceNodes;
            @JsonProperty("interface")
            private String targetInterface;
            @JsonProperty("proto")
            private List<String> proto;
            @JsonProperty("port_list")
            private List<String> portList;
			public List<String> getSourceIps() {
				return sourceIps;
			}
			public void setSourceIps(List<String> sourceIps) {
				this.sourceIps = sourceIps;
			}
			public List<String> getSourceNodes() {
				return sourceNodes;
			}
			public void setSourceNodes(List<String> sourceNodes) {
				this.sourceNodes = sourceNodes;
			}
			public String getTargetInterface() {
				return targetInterface;
			}
			public void setTargetInterface(String targetInterface) {
				this.targetInterface = targetInterface;
			}
			public List<String> getProto() {
				return proto;
			}
			public void setProto(List<String> proto) {
				this.proto = proto;
			}
			public List<String> getPortList() {
				return portList;
			}
			public void setPortList(List<String> portList) {
				this.portList = portList;
			}
            
            
        }

        public static class Outcoming implements Serializable{
            /**
             * 
             */
            private static final long serialVersionUID = -8199070091162031438L;
            @JsonProperty("destination_ips")
            private List<String> destinationIps;
            @JsonProperty("destination_nodes")
            private List<String> destinationNodes;
            @JsonProperty("interface")
            private String targetInterface;
            @JsonProperty("proto")
            private List<String> proto;
            @JsonProperty("port_list")
            private List<String> portList;
        }
    }
}
