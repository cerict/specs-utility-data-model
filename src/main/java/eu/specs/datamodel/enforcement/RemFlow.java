package eu.specs.datamodel.enforcement;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class RemFlow implements Serializable {
    private static final long serialVersionUID = -3389718613982201310L;
    @JsonProperty("name")
    private String name;
    @JsonProperty("action_id")
    private String actionId;
    @JsonProperty("yes_action")
    private Object yesAction;
    @JsonProperty("no_action")
    private Object noAction;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getActionId() {
        return actionId;
    }

    public void setActionId(String actionId) {
        this.actionId = actionId;
    }

    public Object getYesAction() {
        return yesAction;
    }

    public void setYesAction(Object yesAction) {
        this.yesAction = yesAction;
    }

    public Object getNoAction() {
        return noAction;
    }

    public void setNoAction(Object noAction) {
        this.noAction = noAction;
    }
}
