package eu.specs.datamodel.enforcement;

import com.fasterxml.jackson.annotation.JsonProperty;
import eu.specs.datamodel.common.SlaState;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EuNotification {
    @JsonProperty("eu_notification_id")
    private String id;
    @JsonProperty("sla_id")
    private String slaId;
    @JsonProperty("sla_state")
    private SlaState slaState;
    @JsonProperty("creation_time")
    private Date creationTime;
    @JsonProperty("diag_act_id")
    private String diagActId;
    @JsonProperty("component")
    private String component;
    @JsonProperty("object")
    private String object;
    @JsonProperty("measurement_id")
    private String measurementId;
    @JsonProperty("value")
    private String value;
    @JsonProperty("measurement_time")
    private Date measurementTime;
    @JsonProperty("affected_slos")
    private List<String> affectedSlos = new ArrayList<>();
    @JsonProperty("event_impact")
    private int eventImpact;
    @JsonProperty("root_cause")
    private List<String> rootCauses = new ArrayList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSlaId() {
        return slaId;
    }

    public void setSlaId(String slaId) {
        this.slaId = slaId;
    }

    public SlaState getSlaState() {
        return slaState;
    }

    public void setSlaState(SlaState slaState) {
        this.slaState = slaState;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    public String getDiagActId() {
        return diagActId;
    }

    public void setDiagActId(String diagActId) {
        this.diagActId = diagActId;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public String getMeasurementId() {
        return measurementId;
    }

    public void setMeasurementId(String measurementId) {
        this.measurementId = measurementId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Date getMeasurementTime() {
        return measurementTime;
    }

    public void setMeasurementTime(Date measurementTime) {
        this.measurementTime = measurementTime;
    }

    public List<String> getAffectedSlos() {
        return affectedSlos;
    }

    public void setAffectedSlos(List<String> affectedSlos) {
        this.affectedSlos = affectedSlos;
    }

    public int getEventImpact() {
        return eventImpact;
    }

    public void setEventImpact(int eventImpact) {
        this.eventImpact = eventImpact;
    }

    public List<String> getRootCauses() {
        return rootCauses;
    }

    public void setRootCauses(List<String> rootCauses) {
        this.rootCauses = rootCauses;
    }
}
