package eu.specs.datamodel.enforcement;

import com.fasterxml.jackson.annotation.JsonProperty;
import eu.specs.datamodel.common.SlaState;

import java.io.Serializable;
import java.util.Date;

public class CompActivity implements Serializable {
    @JsonProperty("comp_activity_id")
    private String id;
    @JsonProperty("sla_id")
    private String slaId;
    @JsonProperty("sla_state")
    private SlaState slaState;
    @JsonProperty("component_id")
    private String componentId;
    @JsonProperty("component_state")
    private ComponentState componentState;
    @JsonProperty("creation_time")
    private Date creationTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSlaId() {
        return slaId;
    }

    public void setSlaId(String slaId) {
        this.slaId = slaId;
    }

    public SlaState getSlaState() {
        return slaState;
    }

    public void setSlaState(SlaState slaState) {
        this.slaState = slaState;
    }

    public String getComponentId() {
        return componentId;
    }

    public void setComponentId(String componentId) {
        this.componentId = componentId;
    }

    public ComponentState getComponentState() {
        return componentState;
    }

    public void setComponentState(ComponentState componentState) {
        this.componentState = componentState;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    public static enum ComponentState {
        ACTIVATED,
        DEACTIVATED
    }
}
