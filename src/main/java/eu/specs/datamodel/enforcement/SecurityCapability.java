/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
@author  Pasquale De Rosa p.derosa@teamandroid.it
 */

package eu.specs.datamodel.enforcement;


import java.util.Date;

public class SecurityCapability {

    public SecurityCapability(){
        created = updated = new Date();
    }

    int referenceID;
    Date created;
    Date updated;
    String XMLdescription;  
    
    public void setCreated(Date created){
        this.created = created;
    }
    
    public Date getCreated(){
        return created;
    }
    
    public void setUpdated(Date updated){
        this.updated = updated;
    }
    
    public Date getUpdated(){
        return updated;
    }

    public int getReferenceID() {
        return referenceID;
    }

    public void setReferenceID(int referenceID) {
        this.referenceID = referenceID;
    }

    public String getXMLdescription() {
        return XMLdescription;
    }

    public void setXMLdescription(String xMLdescription) {
        XMLdescription = xMLdescription;
    }
}
