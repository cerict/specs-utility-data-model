package eu.specs.datamodel.enforcement;

import java.util.ArrayList;
import java.util.List;

public class CloudResources {
    private String providerId;
    private List<Zone> zones;

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    public List<Zone> getZones() {
        return zones;
    }

    public void setZones(List<Zone> zones) {
        this.zones = zones;
    }

    public void addZone(Zone zone) {
        if (this.zones == null) {
            this.zones = new ArrayList<>();
        }
        this.zones.add(zone);
    }

    public static class Zone {
        private String zoneId;
        private int maxAllowedVMs;
        private List<VmType> vmTypes;

        public String getZoneId() {
            return zoneId;
        }

        public void setZoneId(String zoneId) {
            this.zoneId = zoneId;
        }

        public int getMaxAllowedVMs() {
            return maxAllowedVMs;
        }

        public void setMaxAllowedVMs(int maxAllowedVMs) {
            this.maxAllowedVMs = maxAllowedVMs;
        }

        public List<VmType> getVmTypes() {
            return vmTypes;
        }

        public void setVmTypes(List<VmType> vmTypes) {
            this.vmTypes = vmTypes;
        }

        public void addVmType(VmType vmType) {
            if (this.vmTypes == null) {
                this.vmTypes = new ArrayList<>();
            }
            this.vmTypes.add(vmType);
        }
    }

    public static class VmType {
        private String vmTypeId;
        private String appliance;
        private String hardware;

        public VmType() {
        }

        public VmType(String vmTypeId, String appliance, String hardware) {
            this.vmTypeId = vmTypeId;
            this.appliance = appliance;
            this.hardware = hardware;
        }

        public String getVmTypeId() {
            return vmTypeId;
        }

        public void setVmTypeId(String vmTypeId) {
            this.vmTypeId = vmTypeId;
        }

        public String getAppliance() {
            return appliance;
        }

        public void setAppliance(String appliance) {
            this.appliance = appliance;
        }

        public String getHardware() {
            return hardware;
        }

        public void setHardware(String hardware) {
            this.hardware = hardware;
        }
    }
}
