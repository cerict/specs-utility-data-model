package eu.specs.datamodel.enforcement;

import com.fasterxml.jackson.annotation.JsonProperty;
import eu.specs.datamodel.common.SlaState;

import java.io.Serializable;
import java.util.Date;

public class RemResult implements Serializable {
    @JsonProperty("rem_result_id")
    private String id;
    @JsonProperty("sla_id")
    private String slaId;
    @JsonProperty("sla_state")
    private SlaState slaState;
    @JsonProperty("plan_id")
    private String planId;
    @JsonProperty("result")
    private String result;
    @JsonProperty("creation_time")
    private Date creationTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSlaId() {
        return slaId;
    }

    public void setSlaId(String slaId) {
        this.slaId = slaId;
    }

    public SlaState getSlaState() {
        return slaState;
    }

    public void setSlaState(SlaState slaState) {
        this.slaState = slaState;
    }

    public String getPlanId() {
        return planId;
    }

    public void setPlanId(String planId) {
        this.planId = planId;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }
}
