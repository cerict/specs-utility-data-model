package eu.specs.datamodel.enforcement;

import eu.specs.datamodel.common.Annotation;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PlanningActivity {
    private String id;
    private Date creationTime;
    private Status state;
    private String slaId;
    private String slaState;
    private String supplyChainId;
    private SupplyChain supplyChain;
    private String implActivityId;
    private int planCount;
    private String activePlanId;
    private List<Annotation> annotations;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    public Status getState() {
        return state;
    }

    public void setState(Status state) {
        this.state = state;
    }

    public String getSlaId() {
        return slaId;
    }

    public void setSlaId(String slaId) {
        this.slaId = slaId;
    }

    public String getSlaState() {
        return slaState;
    }

    public void setSlaState(String slaState) {
        this.slaState = slaState;
    }

    public String getSupplyChainId() {
        return supplyChainId;
    }

    public void setSupplyChainId(String supplyChainId) {
        this.supplyChainId = supplyChainId;
    }

    public SupplyChain getSupplyChain() {
        return supplyChain;
    }

    public void setSupplyChain(SupplyChain supplyChain) {
        this.supplyChain = supplyChain;
    }

    public String getImplActivityId() {
        return implActivityId;
    }

    public void setImplActivityId(String implActivityId) {
        this.implActivityId = implActivityId;
    }

    public int getPlanCount() {
        return planCount;
    }

    public void setPlanCount(int planCount) {
        this.planCount = planCount;
    }

    public String getActivePlanId() {
        return activePlanId;
    }

    public void setActivePlanId(String activePlanId) {
        this.activePlanId = activePlanId;
    }

    public List<Annotation> getAnnotations() {
        return annotations;
    }

    public void setAnnotations(List<Annotation> annotations) {
        this.annotations = annotations;
    }

    public void addAnnotation(Annotation annotation) {
        if (this.annotations == null) {
            this.annotations = new ArrayList<>();
        }
        this.annotations.add(annotation);
    }

    public static enum Status {
        CREATED,
        BUILDING,
        IMPLEMENTING,
        ACTIVE,
        TERMINATED,
        ERROR
    }
}
