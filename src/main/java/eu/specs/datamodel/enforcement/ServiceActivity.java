package eu.specs.datamodel.enforcement;

import com.fasterxml.jackson.annotation.JsonProperty;
import eu.specs.datamodel.common.SlaState;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class ServiceActivity implements Serializable {
    @JsonProperty("serv_activity_id")
    private String id;
    @JsonProperty("sla_id")
    private String slaId;
    @JsonProperty("sla_state")
    private SlaState slaState;
    @JsonProperty("impl_plan_id")
    private String implPlanId;
    @JsonProperty("services")
    private List<String> services;
    @JsonProperty("creation_time")
    private Date creationTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSlaId() {
        return slaId;
    }

    public void setSlaId(String slaId) {
        this.slaId = slaId;
    }

    public SlaState getSlaState() {
        return slaState;
    }

    public void setSlaState(SlaState slaState) {
        this.slaState = slaState;
    }

    public String getImplPlanId() {
        return implPlanId;
    }

    public void setImplPlanId(String implPlanId) {
        this.implPlanId = implPlanId;
    }

    public List<String> getServices() {
        return services;
    }

    public void setServices(List<String> services) {
        this.services = services;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }
}
