package eu.specs.datamodel.enforcement;

import com.fasterxml.jackson.annotation.JsonProperty;
import eu.specs.datamodel.common.Annotation;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ImplementationPlanOneProvider {
	@JsonProperty("plan_id")
	private String id;
	@JsonProperty("plan_act_id")
	private String planningActivityId;
	@JsonProperty("sla_id")
	private String slaId;
	@JsonProperty("supply_chain_id")
	private String supplyChainId;
	@JsonProperty("creation_time")
	private Date creationTime;
	@JsonProperty("monitoring_core_ip")
	private String monitoringCoreIp;
	@JsonProperty("monitoring_core_port")
	private int monitoringCorePort;
	@JsonProperty("iaas")
	private Iaas iaas;
	@JsonProperty("pools")
	private List<Pool> pools = new ArrayList<>();
	@JsonProperty("slos")
	private List<Slo> slos = new ArrayList<>();
	@JsonProperty("measurements")
	private List<Measurement> measurements = new ArrayList<>();
	@JsonProperty("annotations")
	private List<Annotation> annotations = new ArrayList<>();

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPlanningActivityId() {
		return planningActivityId;
	}

	public void setPlanningActivityId(String planningActivityId) {
		this.planningActivityId = planningActivityId;
	}

	public String getSlaId() {
		return slaId;
	}

	public void setSlaId(String slaId) {
		this.slaId = slaId;
	}

	public String getSupplyChainId() {
		return supplyChainId;
	}

	public void setSupplyChainId(String supplyChainId) {
		this.supplyChainId = supplyChainId;
	}

	public Date getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Date creationTime) {
		this.creationTime = creationTime;
	}

	public String getMonitoringCoreIp() {
		return monitoringCoreIp;
	}

	public void setMonitoringCoreIp(String monitoringCoreIp) {
		this.monitoringCoreIp = monitoringCoreIp;
	}

	public int getMonitoringCorePort() {
		return monitoringCorePort;
	}

	public void setMonitoringCorePort(int monitoringCorePort) {
		this.monitoringCorePort = monitoringCorePort;
	}

	public Iaas getIaas() {
		return iaas;
	}

	public void setIaas(Iaas iaas) {
		this.iaas = iaas;
	}

	public List<Pool> getPools() {
		return pools;
	}

	public void setPools(List<Pool> pools) {
		this.pools = pools;
	}

	public void addPool(Pool pool) {
		this.pools.add(pool);
	}

	public List<Slo> getSlos() {
		return slos;
	}

	public void setSlos(List<Slo> slos) {
		this.slos = slos;
	}

	public void addSlo(Slo slo) {
		this.slos.add(slo);
	}

	public List<Measurement> getMeasurements() {
		return measurements;
	}

	public void addMeasurement(Measurement measurement) {
		this.measurements.add(measurement);
	}

	public void setMeasurements(List<Measurement> measurements) {
		this.measurements = measurements;
	}

	public List<Annotation> getAnnotations() {
		return annotations;
	}

	public void setAnnotations(List<Annotation> annotations) {
		this.annotations = annotations;
	}

	public void addAnnotation(Annotation annotation) {
		this.annotations.add(annotation);
	}

	public static class Iaas {
		@JsonProperty("provider")
		private String provider;
		@JsonProperty("zone")
		private String zone;
		@JsonProperty("appliance")
		private String appliance;
		@JsonProperty("hardware")
		private String hardware;

		public String getProvider() {
			return provider;
		}

		public void setProvider(String provider) {
			this.provider = provider;
		}

		public String getZone() {
			return zone;
		}

		public void setZone(String zone) {
			this.zone = zone;
		}

		public String getAppliance() {
			return appliance;
		}

		public void setAppliance(String appliance) {
			this.appliance = appliance;
		}

		public String getHardware() {
			return hardware;
		}

		public void setHardware(String hardware) {
			this.hardware = hardware;
		}
	}

	public static class Pool {
		@JsonProperty("pool_name")
		private String poolName;
		@JsonProperty("pool_seq_num")
		private int poolSeqNum;
		@JsonProperty("nodes_access_credentials_reference")
		private String nodesAccessCredentialsReference;
		@JsonProperty("vms")
		private List<Vm> vms = new ArrayList<>();

		public String getPoolName() {
			return poolName;
		}

		public void setPoolName(String poolName) {
			this.poolName = poolName;
		}

		public int getPoolSeqNum() {
			return poolSeqNum;
		}

		public void setPoolSeqNum(int poolSeqNum) {
			this.poolSeqNum = poolSeqNum;
		}

		public String getNodesAccessCredentialsReference() {
			return nodesAccessCredentialsReference;
		}

		public void setNodesAccessCredentialsReference(String nodesAccessCredentialsReference) {
			this.nodesAccessCredentialsReference = nodesAccessCredentialsReference;
		}

		public List<Vm> getVms() {
			return vms;
		}

		public void setVms(List<Vm> vms) {
			this.vms = vms;
		}

		public void addVm(Vm vm) {
			this.vms.add(vm);
		}
	}

	public static class Vm {
		@JsonProperty("vm_seq_num")
		private int vmSeqNum;
		@JsonProperty("public_ip")
		private String publicIp;
		@JsonProperty("components")
		private List<Component> components = new ArrayList<>();

		public int getVmSeqNum() {
			return vmSeqNum;
		}

		public void setVmSeqNum(int vmSeqNum) {
			this.vmSeqNum = vmSeqNum;
		}

		public String getPublicIp() {
			return publicIp;
		}

		public void setPublicIp(String publicIp) {
			this.publicIp = publicIp;
		}

		public List<Component> getComponents() {
			return components;
		}

		public void setComponents(List<Component> components) {
			this.components = components;
		}

		public void addComponent(Component component) {
			this.components.add(component);
		}
	}

	public static class Component {

		@JsonProperty("component_id")
		private String id;
		@JsonProperty("cookbook")
		private String cookbook;
		@JsonProperty("recipe")
		private String recipe;
		@JsonProperty("implementation_step")
		private int implementationStep;
		@JsonProperty("acquire_public_ip")
		private boolean acquirePublicIp;
		@JsonProperty("private_ips_count")
		private int privateIpsCount;
		@JsonProperty("firewall")
		private eu.specs.datamodel.enforcement.Component.Firewall firewall;
		@JsonProperty("private_ips")
		private List<String> privateIps = new ArrayList<>();
		@JsonProperty("status")
		private String status;

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getCookbook() {
			return cookbook;
		}

		public void setCookbook(String cookbook) {
			this.cookbook = cookbook;
		}


		public int getImplementationStep() {
			return implementationStep;
		}

		public String getRecipe() {
			return recipe;
		}

		public void setRecipe(String recipe) {
			this.recipe = recipe;
		}

		public void setImplementationStep(int implementationStep) {
			this.implementationStep = implementationStep;
		}

		public boolean isAcquirePublicIp() {
			return acquirePublicIp;
		}

		public void setAcquirePublicIp(boolean acquirePublicIp) {
			this.acquirePublicIp = acquirePublicIp;
		}

		public int getPrivateIpsCount() {
			return privateIpsCount;
		}

		public void setPrivateIpsCount(int privateIpsCount) {
			this.privateIpsCount = privateIpsCount;
		}

		public eu.specs.datamodel.enforcement.Component.Firewall getFirewall() {
			return firewall;
		}

		public void setFirewall(eu.specs.datamodel.enforcement.Component.Firewall firewall) {
			this.firewall = firewall;
		}

		public List<String> getPrivateIps() {
			return privateIps;
		}

		public void setPrivateIps(List<String> privateIps) {
			this.privateIps = privateIps;
		}

		public void addPrivateIp(String privateIp) {
			this.privateIps.add(privateIp);
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}
	}
}
