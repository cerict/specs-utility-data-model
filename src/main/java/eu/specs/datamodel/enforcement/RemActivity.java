package eu.specs.datamodel.enforcement;

import com.fasterxml.jackson.annotation.JsonProperty;
import eu.specs.datamodel.common.Annotation;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RemActivity {
    @JsonProperty("rem_act_id")
    private String id;
    @JsonProperty("state")
    private Status state;
    @JsonProperty("creation_time")
    private Date creationTime;
    @JsonProperty("diag_act_id")
    private String diagActivityId;
    @JsonProperty("component")
    private String component;
    @JsonProperty("object")
    private String object;
    @JsonProperty("sla_id")
    private String slaId;
    @JsonProperty("plan_id")
    private String planId;
    @JsonProperty("measurement_id")
    private String measurementId;
    @JsonProperty("measurement_time")
    private Date measurementTime;
    @JsonProperty("event_id")
    private String eventId;
    @JsonProperty("value")
    private String value;
    @JsonProperty("affected_slos")
    private List<String> affectedSlos = new ArrayList<>();
    @JsonProperty("classification")
    private Classification classification;
    @JsonProperty("event_impact")
    private int eventImpact;
    @JsonProperty("root_causes")
    private List<String> rootCauses = new ArrayList<>();
    @JsonProperty("priority_queue_timestamp")
    private Date priorityQueueTimestamp;
    @JsonProperty("rem_plan_id")
    private String remPlanId;
    @JsonProperty("result")
    private RemPlan.Result result;
    @JsonProperty("annotations")
    private List<Annotation> annotations = new ArrayList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Status getState() {
        return state;
    }

    public void setState(Status state) {
        this.state = state;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    public String getDiagActivityId() {
        return diagActivityId;
    }

    public void setDiagActivityId(String diagActivityId) {
        this.diagActivityId = diagActivityId;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public String getSlaId() {
        return slaId;
    }

    public void setSlaId(String slaId) {
        this.slaId = slaId;
    }

    public String getPlanId() {
        return planId;
    }

    public void setPlanId(String planId) {
        this.planId = planId;
    }

    public String getMeasurementId() {
        return measurementId;
    }

    public void setMeasurementId(String measurementId) {
        this.measurementId = measurementId;
    }

    public Date getMeasurementTime() {
        return measurementTime;
    }

    public void setMeasurementTime(Date measurementTime) {
        this.measurementTime = measurementTime;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public List<String> getAffectedSlos() {
        return affectedSlos;
    }

    public void setAffectedSlos(List<String> affectedSlos) {
        this.affectedSlos = affectedSlos;
    }

    public Classification getClassification() {
        return classification;
    }

    public void setClassification(Classification classification) {
        this.classification = classification;
    }

    public int getEventImpact() {
        return eventImpact;
    }

    public void setEventImpact(int eventImpact) {
        this.eventImpact = eventImpact;
    }

    public List<String> getRootCauses() {
        return rootCauses;
    }

    public void setRootCauses(List<String> rootCauses) {
        this.rootCauses = rootCauses;
    }

    public Date getPriorityQueueTimestamp() {
        return priorityQueueTimestamp;
    }

    public void setPriorityQueueTimestamp(Date priorityQueueTimestamp) {
        this.priorityQueueTimestamp = priorityQueueTimestamp;
    }

    public String getRemPlanId() {
        return remPlanId;
    }

    public void setRemPlanId(String remPlanId) {
        this.remPlanId = remPlanId;
    }

    public RemPlan.Result getResult() {
        return result;
    }

    public void setResult(RemPlan.Result result) {
        this.result = result;
    }

    public List<Annotation> getAnnotations() {
        return annotations;
    }

    public void setAnnotations(List<Annotation> annotations) {
        this.annotations = annotations;
    }

    public void addAnnotation(Annotation annotation) {
        this.annotations.add(annotation);
    }

    public static enum Status {
        CREATED,
        REMEDIATING,
        SOLVED,
        ERROR
    }

    public static enum Classification {
        FALSE_POSITIVE,
        ALERT,
        VIOLATION
    }
}
