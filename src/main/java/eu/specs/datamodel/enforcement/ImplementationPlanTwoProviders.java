package eu.specs.datamodel.enforcement;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import eu.specs.datamodel.enforcement.Component.Firewall;
import eu.specs.datamodel.enforcement.Component.VmRequirement;
import eu.specs.datamodel.enforcement.ImplementationPlanOneProvider.Component;
import eu.specs.datamodel.enforcement.ImplementationPlanOneProvider.Pool;
import eu.specs.datamodel.enforcement.ImplementationPlanOneProvider.Vm;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "plan_id",
    "sla_id",
    "creation_time",
    "monitoring_core_ip",
    "monitoring_core_port",
    "csps",
    "slos",
    "measurements",
    "annotations"
})


public class ImplementationPlanTwoProviders {

    @JsonProperty("plan_id")
    private String id;

    @JsonProperty("sla_id")
    private String slaId;

    @JsonProperty("creation_time")
    private Date creationTime;
    @JsonProperty("monitoring_core_ip")
    private String monitoringCoreIp;
    @JsonProperty("monitoring_core_port")
    private Integer monitoringCorePort;
	@JsonProperty("plan_act_id")
	private String planningActivityId;
	@JsonProperty("supply_chain_id")
	private String supplyChainId;
    @JsonProperty("csps")
    private List<Csp> csps = new ArrayList<Csp>();
    @JsonProperty("slos")
    private List<Slo> slos = new ArrayList<Slo>();
    @JsonProperty("measurements")
    private List<Measurement> measurements = new ArrayList<Measurement>();
    @JsonProperty("annotations")
    private List<Object> annotations = new ArrayList<Object>();

    /**
     * 
     * @return
     *     The planId
     */
    @JsonProperty("plan_id")
    public String getId() {
        return id;
    }

    /**
     * 
     * @param planId
     *     The plan_id
     */
    @JsonProperty("plan_id")
    public void setId(String id) {
        this.id = id;
    }


    /**
     * 
     * @return
     *     The slaId
     */
    @JsonProperty("sla_id")
    public String getSlaId() {
        return slaId;
    }

    /**
     * 
     * @param slaId
     *     The sla_id
     */
    @JsonProperty("sla_id")
    public void setSlaId(String slaId) {
        this.slaId = slaId;
    }


    /**
     * 
     * @return
     *     The creationTime
     */
    @JsonProperty("creation_time")
    public Date getCreationTime() {
        return creationTime;
    }

    /**
     * 
     * @param creationTime
     *     The creation_time
     */
    @JsonProperty("creation_time")
    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    /**
     * 
     * @return
     *     The monitoringCoreIp
     */
    @JsonProperty("monitoring_core_ip")
    public String getMonitoringCoreIp() {
        return monitoringCoreIp;
    }

    /**
     * 
     * @param monitoringCoreIp
     *     The monitoring_core_ip
     */
    @JsonProperty("monitoring_core_ip")
    public void setMonitoringCoreIp(String monitoringCoreIp) {
        this.monitoringCoreIp = monitoringCoreIp;
    }

    /**
     * 
     * @return
     *     The monitoringCorePort
     */
    @JsonProperty("monitoring_core_port")
    public Integer getMonitoringCorePort() {
        return monitoringCorePort;
    }

    /**
     * 
     * @param monitoringCorePort
     *     The monitoring_core_port
     */
    @JsonProperty("monitoring_core_port")
    public void setMonitoringCorePort(Integer monitoringCorePort) {
        this.monitoringCorePort = monitoringCorePort;
    }

    /**
     * 
     * @return
     *     The csps
     */
    @JsonProperty("csps")
    public List<Csp> getCsps() {
        return csps;
    }

    /**
     * 
     * @param csps
     *     The csps
     */
    @JsonProperty("csps")
    public void setCsps(List<Csp> csps) {
        this.csps = csps;
    }

    /**
     * 
     * @return
     *     The slos
     */
    @JsonProperty("slos")
    public List<Slo> getSlos() {
        return slos;
    }

    /**
     * 
     * @param list
     *     The slos
     */
    @JsonProperty("slos")
    public void setSlos(List<Slo> list) {
        this.slos = list;
    }

    /**
     * 
     * @return
     *     The measurements
     */
    @JsonProperty("measurements")
    public List<Measurement> getMeasurements() {
        return measurements;
    }

    /**
     * 
     * @param measurements
     *     The measurements
     */
    @JsonProperty("measurements")
    public void setMeasurements(List<Measurement> measurements) {
        this.measurements = measurements;
    }

    /**
     * 
     * @return
     *     The annotations
     */
    @JsonProperty("annotations")
    public List<Object> getAnnotations() {
        return annotations;
    }

    /**
     * 
     * @param annotations
     *     The annotations
     */
    @JsonProperty("annotations")
    public void setAnnotations(List<Object> annotations) {
        this.annotations = annotations;
    }
    
    
	public void addMeasurement(Measurement measurement) {
		this.measurements.add(measurement);
	}
	
	public String getPlanningActivityId() {
		return planningActivityId;
	}

	public void setPlanningActivityId(String planningActivityId) {
		this.planningActivityId = planningActivityId;
	}
	
	public String getSupplyChainId() {
		return supplyChainId;
	}

	public void setSupplyChainId(String supplyChainId) {
		this.supplyChainId = supplyChainId;
	}
	
	
	
	
    
    
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Generated("org.jsonschema2pojo")
    @JsonPropertyOrder({
        "component_id",
        "cookbook",
        "recipe",
        "implementation_step",
        "acquire_public_ip",
        "firewall",
        "private_ips",
        "status"
    })
    public static class Component {

    	@JsonProperty("component_name")
        private String name;
        @JsonProperty("vm_id")
        private String vmId;
        @JsonProperty("component_id")
        private String componentId;
        @JsonProperty("cookbook")
        private String cookbook;
        @JsonProperty("recipe")
        private String recipe;
        @JsonProperty("implementation_step")
        private Integer implementationStep;
        @JsonProperty("acquire_public_ip")
        private Boolean acquirePublicIp;
        @JsonProperty("firewall")
        private Firewall firewall;
        @JsonProperty("private_ips")
        private List<String> privateIps = new ArrayList<String>();
        @JsonProperty("status")
        private Object status;
        @JsonProperty("vm_requirement")
        private VmRequirement vmRequirement;

        /**
         * 
         * @return
         *     The componentId
         */
        @JsonProperty("component_id")
        public String getComponentId() {
            return componentId;
        }

        /**
         * 
         * @param componentId
         *     The component_id
         */
        @JsonProperty("component_id")
        public void setComponentId(String componentId) {
            this.componentId = componentId;
        }
        
        
        @JsonProperty("vm_id")
        public String getVmId() {
			return vmId;
		}
        @JsonProperty("vm_id")
		public void setVmId(String vmId) {
			this.vmId = vmId;
		}
        
        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
        
        public VmRequirement getVmRequirement() {
            return vmRequirement;
        }

        public void setVmRequirement(VmRequirement vmRequirement) {
            this.vmRequirement = vmRequirement;
        }

        /**
         * 
         * @return
         *     The cookbook
         */
        @JsonProperty("cookbook")
        public String getCookbook() {
            return cookbook;
        }

        /**
         * 
         * @param cookbook
         *     The cookbook
         */
        @JsonProperty("cookbook")
        public void setCookbook(String cookbook) {
            this.cookbook = cookbook;
        }

        /**
         * 
         * @return
         *     The recipe
         */
        @JsonProperty("recipe")
        public String getRecipe() {
            return recipe;
        }

        /**
         * 
         * @param recipe
         *     The recipe
         */
        @JsonProperty("recipe")
        public void setRecipe(String recipe) {
            this.recipe = recipe;
        }

        /**
         * 
         * @return
         *     The implementationStep
         */
        @JsonProperty("implementation_step")
        public Integer getImplementationStep() {
            return implementationStep;
        }

        /**
         * 
         * @param implementationStep
         *     The implementation_step
         */
        @JsonProperty("implementation_step")
        public void setImplementationStep(Integer implementationStep) {
            this.implementationStep = implementationStep;
        }

        /**
         * 
         * @return
         *     The acquirePublicIp
         */
        @JsonProperty("acquire_public_ip")
        public Boolean getAcquirePublicIp() {
            return acquirePublicIp;
        }

        /**
         * 
         * @param acquirePublicIp
         *     The acquire_public_ip
         */
        @JsonProperty("acquire_public_ip")
        public void setAcquirePublicIp(Boolean acquirePublicIp) {
            this.acquirePublicIp = acquirePublicIp;
        }


        /**
         * 
         * @return
         *     The firewall
         */
        @JsonProperty("firewall")
        public Firewall getFirewall() {
            return firewall;
        }

        /**
         * 
         * @param firewall
         *     The firewall
         */
        @JsonProperty("firewall")
        public void setFirewall(Firewall firewall) {
            this.firewall = firewall;
        }

        /**
         * 
         * @return
         *     The privateIps
         */
        @JsonProperty("private_ips")
        public List<String> getPrivateIps() {
            return privateIps;
        }

        /**
         * 
         * @param privateIps
         *     The private_ips
         */
        @JsonProperty("private_ips")
        public void setPrivateIps(List<String> privateIps) {
            this.privateIps = privateIps;
        }
        
        public void addPrivateIp(String privateIp) {
			this.privateIps.add(privateIp);
		}

        /**
         * 
         * @return
         *     The status
         */
        @JsonProperty("status")
        public Object getStatus() {
            return status;
        }

        /**
         * 
         * @param status
         *     The status
         */
        @JsonProperty("status")
        public void setStatus(Object status) {
            this.status = status;
        }


    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Generated("org.jsonschema2pojo")
    @JsonPropertyOrder({
        "iaas",
        "pools"
    })
    public static class Csp {

        @JsonProperty("iaas")
        private Iaas iaas;
        
        @JsonProperty("paas")
        private Paas paas;
        
        @JsonProperty("pools")
        private List<Pool> pools = new ArrayList<Pool>();

        /**
         * 
         * @return
         *     The iaas
         */
        @JsonProperty("iaas")
        public Iaas getIaas() {
            return iaas;
        }

        /**
         * 
         * @param iaas
         *     The iaas
         */
        @JsonProperty("iaas")
        public void setIaas(Iaas iaas) {
            this.iaas = iaas;
        }        
        
        public Paas getPaas() {
			return paas;
		}

		public void setPaas(Paas paas) {
			this.paas = paas;
		}

		/**
         * 
         * @return
         *     The pools
         */
        @JsonProperty("pools")
        public List<Pool> getPools() {
            return pools;
        }

        /**
         * 
         * @param pools
         *     The pools
         */
        @JsonProperty("pools")
        public void setPools(List<Pool> pools) {
            this.pools = pools;
        }
        
    	public void addPool(Pool pool) {
    		this.pools.add(pool);
    	}


    }
    
    public static class Paas {

        @JsonProperty("paasAgent")
        private String paasAgent;
        
        @JsonProperty("allocationStrategy")
        private String allocationStrategy;
        
        @JsonProperty("components")
        private List<Component> components = new ArrayList<Component>();
        
        
		public String getPaasAgent() {
			return paasAgent;
		}
		public void setPaasAgent(String paasAgent) {
			this.paasAgent = paasAgent;
		}
		
		public String getAllocationStrategy() {
			return allocationStrategy;
		}
		public void setAllocationStrategy(String allocationStrategy) {
			this.allocationStrategy = allocationStrategy;
		}
		public List<Component> getComponents() {
			return components;
		}
		public void setComponents(List<Component> components) {
			this.components = components;
		}

    }

    
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Generated("org.jsonschema2pojo")
    @JsonPropertyOrder({
        "incoming",
        "outcoming"
    })
    public static class Firewall {

        @JsonProperty("incoming")
        private Incoming incoming;
        @JsonProperty("outcoming")
        private Outcoming outcoming;

        /**
         * 
         * @return
         *     The incoming
         */
        @JsonProperty("incoming")
        public Incoming getIncoming() {
            return incoming;
        }

        /**
         * 
         * @param incoming
         *     The incoming
         */
        @JsonProperty("incoming")
        public void setIncoming(Incoming incoming) {
            this.incoming = incoming;
        }

        /**
         * 
         * @return
         *     The outcoming
         */
        @JsonProperty("outcoming")
        public Outcoming getOutcoming() {
            return outcoming;
        }

        /**
         * 
         * @param outcoming
         *     The outcoming
         */
        @JsonProperty("outcoming")
        public void setOutcoming(Outcoming outcoming) {
            this.outcoming = outcoming;
        }


    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Generated("org.jsonschema2pojo")
    @JsonPropertyOrder({
        "provider",
        "zone"
    })
    public static class Iaas {

        @JsonProperty("provider")
        private String provider;
        
        @JsonProperty("zone")
        private String zone;
        
        @JsonProperty("user")
        private String user;
        
        @JsonProperty("network")
        private String network;

        /**
         * 
         * @return
         *     The provider
         */
        @JsonProperty("provider")
        public String getProvider() {
            return provider;
        }

        /**
         * 
         * @param provider
         *     The provider
         */
        @JsonProperty("provider")
        public void setProvider(String provider) {
            this.provider = provider;
        }
        


        /**
         * 
         * @return
         *     The zone
         */
        @JsonProperty("zone")
        public String getZone() {
            return zone;
        }

        /**
         * 
         * @param zone
         *     The zone
         */
        @JsonProperty("user")
        public void setUser(String user) {
            this.user = user;
        }
        
        /**
         * 
         * @return
         *     The zone
         */
        @JsonProperty("user")
        public String getUser() {
            return user;
        }
        
        
        
        /**
         * 
         * @param network
         *     The network
         */
        @JsonProperty("network")
        public void setNetwork(String network) {
            this.network = network;
        }
        
        /**
         * 
         * @return
         *     The zone
         */
        @JsonProperty("network")
        public String getNetwork() {
            return network;
        }

        /**
         * 
         * @param zone
         *     The zone
         */
        @JsonProperty("zone")
        public void setZone(String zone) {
            this.zone = zone;
        }


    }

    
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Generated("org.jsonschema2pojo")
    @JsonPropertyOrder({
        "source_ips",
        "source_nodes",
        "interface",
        "proto",
        "port_list"
    })
    public static class Incoming {

        @JsonProperty("source_ips")
        private List<Object> sourceIps = new ArrayList<Object>();
        @JsonProperty("source_nodes")
        private List<Object> sourceNodes = new ArrayList<Object>();
        @JsonProperty("interface")
        private String _interface;
        @JsonProperty("proto")
        private List<Proto> proto = new ArrayList<Proto>();


        /**
         * 
         * @return
         *     The sourceIps
         */
        @JsonProperty("source_ips")
        public List<Object> getSourceIps() {
            return sourceIps;
        }

        /**
         * 
         * @param sourceIps
         *     The source_ips
         */
        @JsonProperty("source_ips")
        public void setSourceIps(List<Object> sourceIps) {
            this.sourceIps = sourceIps;
        }

        /**
         * 
         * @return
         *     The sourceNodes
         */
        @JsonProperty("source_nodes")
        public List<Object> getSourceNodes() {
            return sourceNodes;
        }

        /**
         * 
         * @param sourceNodes
         *     The source_nodes
         */
        @JsonProperty("source_nodes")
        public void setSourceNodes(List<Object> sourceNodes) {
            this.sourceNodes = sourceNodes;
        }

        /**
         * 
         * @return
         *     The _interface
         */
        @JsonProperty("interface")
        public String getInterface() {
            return _interface;
        }

        /**
         * 
         * @param _interface
         *     The interface
         */
        @JsonProperty("interface")
        public void setInterface(String _interface) {
            this._interface = _interface;
        }

        /**
         * 
         * @return
         *     The proto
         */
        @JsonProperty("proto")
        public List<Proto> getProto() {
            return proto;
        }

        /**
         * 
         * @param proto
         *     The proto
         */
        @JsonProperty("proto")
        public void setProto(List<Proto> proto) {
            this.proto = proto;
        }

    }

    
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Generated("org.jsonschema2pojo")
    @JsonPropertyOrder({
        "destination_ips",
        "destination_nodes",
        "interface",
        "proto",
        "port_list"
    })
    public static class Outcoming {

        @JsonProperty("destination_ips")
        private List<Object> destinationIps = new ArrayList<Object>();
        @JsonProperty("destination_nodes")
        private List<Object> destinationNodes = new ArrayList<Object>();
        @JsonProperty("interface")
        private String _interface;
        @JsonProperty("proto")
        private List<Proto> proto;

        /**
         * 
         * @return
         *     The destinationIps
         */
        @JsonProperty("destination_ips")
        public List<Object> getDestinationIps() {
            return destinationIps;
        }

        /**
         * 
         * @param destinationIps
         *     The destination_ips
         */
        @JsonProperty("destination_ips")
        public void setDestinationIps(List<Object> destinationIps) {
            this.destinationIps = destinationIps;
        }

        /**
         * 
         * @return
         *     The destinationNodes
         */
        @JsonProperty("destination_nodes")
        public List<Object> getDestinationNodes() {
            return destinationNodes;
        }

        /**
         * 
         * @param destinationNodes
         *     The destination_nodes
         */
        @JsonProperty("destination_nodes")
        public void setDestinationNodes(List<Object> destinationNodes) {
            this.destinationNodes = destinationNodes;
        }

        /**
         * 
         * @return
         *     The _interface
         */
        @JsonProperty("interface")
        public String getInterface() {
            return _interface;
        }

        /**
         * 
         * @param _interface
         *     The interface
         */
        @JsonProperty("interface")
        public void setInterface(String _interface) {
            this._interface = _interface;
        }

        /**
         * 
         * @return
         *     The proto
         */
        @JsonProperty("proto")
        public List<Proto> getProto() {
            return proto;
        }

        /**
         * 
         * @param proto
         *     The proto
         */
        @JsonProperty("proto")
        public void setProto(List<Proto> proto) {
            this.proto = proto;
        }



    }

    
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Generated("org.jsonschema2pojo")
    @JsonPropertyOrder({
        "pool_name",
        "pool_seq_num",
        "nodes_access_credentials_reference",
        "network",
        "vms"
    })
    public static class Pool {

        @JsonProperty("pool_name")
        private Object poolName;
        @JsonProperty("pool_seq_num")
        private Integer poolSeqNum;
        @JsonProperty("nodes_access_credentials_reference")
        private Object nodesAccessCredentialsReference;
        @JsonProperty("network")
        private String network;
        @JsonProperty("vms")
        private List<Vm> vms = new ArrayList<Vm>();

        /**
         * 
         * @return
         *     The poolName
         */
        @JsonProperty("pool_name")
        public Object getPoolName() {
            return poolName;
        }

        /**
         * 
         * @param poolName
         *     The pool_name
         */
        @JsonProperty("pool_name")
        public void setPoolName(Object poolName) {
            this.poolName = poolName;
        }

        /**
         * 
         * @return
         *     The poolSeqNum
         */
        @JsonProperty("pool_seq_num")
        public Integer getPoolSeqNum() {
            return poolSeqNum;
        }

        /**
         * 
         * @param poolSeqNum
         *     The pool_seq_num
         */
        @JsonProperty("pool_seq_num")
        public void setPoolSeqNum(Integer poolSeqNum) {
            this.poolSeqNum = poolSeqNum;
        }

        /**
         * 
         * @return
         *     The nodesAccessCredentialsReference
         */
        @JsonProperty("nodes_access_credentials_reference")
        public Object getNodesAccessCredentialsReference() {
            return nodesAccessCredentialsReference;
        }

        /**
         * 
         * @param nodesAccessCredentialsReference
         *     The nodes_access_credentials_reference
         */
        @JsonProperty("nodes_access_credentials_reference")
        public void setNodesAccessCredentialsReference(Object nodesAccessCredentialsReference) {
            this.nodesAccessCredentialsReference = nodesAccessCredentialsReference;
        }

        /**
         * 
         * @return
         *     The network
         */
        @JsonProperty("network")
        public String getNetwork() {
            return network;
        }

        /**
         * 
         * @param network
         *     The network
         */
        @JsonProperty("network")
        public void setNetwork(String network) {
            this.network = network;
        }

        /**
         * 
         * @return
         *     The vms
         */
        @JsonProperty("vms")
        public List<Vm> getVms() {
            return vms;
        }

        /**
         * 
         * @param vms
         *     The vms
         */
        @JsonProperty("vms")
        public void setVms(List<Vm> vms) {
            this.vms = vms;
        }
        
        public void addVm(Vm vm) {
			this.vms.add(vm);
		}


    }

    
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Generated("org.jsonschema2pojo")
    @JsonPropertyOrder({
        "vm_seq_num",
        "appliance",
        "hardware",
        "public_ip",
        "components"
    })
    public static class Vm {

        @JsonProperty("vm_seq_num")
        private Integer vmSeqNum;
        @JsonProperty("nic")
        private String nic;
        @JsonProperty("appliance")
        private String appliance;
        @JsonProperty("hardware")
        private String hardware;
        @JsonProperty("public_ip")
        private String publicIp;
        @JsonProperty("vm_id")
        private String vmId;
        @JsonProperty("components")
        private List<Component> components = new ArrayList<Component>();

        /**
         * 
         * @return
         *     The vmSeqNum
         */
        @JsonProperty("vm_seq_num")
        public Integer getVmSeqNum() {
            return vmSeqNum;
        }

        /**
         * 
         * @param vmSeqNum
         *     The vm_seq_num
         */
        @JsonProperty("vm_seq_num")
        public void setVmSeqNum(Integer vmSeqNum) {
            this.vmSeqNum = vmSeqNum;
        }
                
        
        public String getNic() {
			return nic;
		}

		public void setNic(String nic) {
			this.nic = nic;
		}

		@JsonProperty("vm_id")
        public String getVmId() {
			return vmId;
		}
        @JsonProperty("vm_id")
		public void setVmId(String vmId) {
			this.vmId = vmId;
		}

		/**
         * 
         * @return
         *     The appliance
         */
        @JsonProperty("appliance")
        public String getAppliance() {
            return appliance;
        }

        /**
         * 
         * @param appliance
         *     The appliance
         */
        @JsonProperty("appliance")
        public void setAppliance(String appliance) {
            this.appliance = appliance;
        }

        /**
         * 
         * @return
         *     The hardware
         */
        @JsonProperty("hardware")
        public String getHardware() {
            return hardware;
        }

        /**
         * 
         * @param hardware
         *     The hardware
         */
        @JsonProperty("hardware")
        public void setHardware(String hardware) {
            this.hardware = hardware;
        }

        /**
         * 
         * @return
         *     The publicIp
         */
        @JsonProperty("public_ip")
        public String getPublicIp() {
            return publicIp;
        }

        /**
         * 
         * @param publicIp
         *     The public_ip
         */
        @JsonProperty("public_ip")
        public void setPublicIp(String publicIp) {
            this.publicIp = publicIp;
        }

        /**
         * 
         * @return
         *     The components
         */
        @JsonProperty("components")
        public List<Component> getComponents() {
            return components;
        }

        /**
         * 
         * @param components
         *     The components
         */
        @JsonProperty("components")
        public void setComponents(List<Component> components) {
            this.components = components;
        }
        
        public void addComponent(Component component) {
			this.components.add(component);
		}


    }
    
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonPropertyOrder({
        "port_list",
        "type"
    })
    public static class Proto{
    	@JsonProperty("port_list")
        private List<String> port_list;

    	@JsonProperty("type")
        private String type;

    	public Proto(){
    		port_list=new ArrayList<String>();
    	}
    	
    	@JsonProperty("port_list")
        public List<String> getPort_list ()
        {
            return port_list;
        }

    	@JsonProperty("port_list")
        public void setPort_list (List<String> port_list)
        {
            this.port_list = port_list;
        }
    	
    	@JsonProperty("type")
        public String getType ()
        {
            return type;
        }
    	
    	@JsonProperty("type")
        public void setType (String type)
        {
            this.type = type;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [port_list = "+port_list+", type = "+type+"]";
        }
    }
    

    public static class VmRequirement implements Serializable{
        /**
         * 
         */
        private static final long serialVersionUID = 4030075570750680452L;
        @JsonProperty("hardware")
        private String hardware;
        @JsonProperty("usage")
        private String usage;
        @JsonProperty("acquire_public_ip")
        private boolean acquirePublicIp;
        @JsonProperty("private_ips_count")
        private int privateIpsCount;
        @JsonProperty("firewall")
        private Firewall firewall;

        public String getHardware() {
            return hardware;
        }

        public void setHardware(String hardware) {
            this.hardware = hardware;
        }

        public String getUsage() {
            return usage;
        }

        public void setUsage(String usage) {
            this.usage = usage;
        }

        public boolean isAcquirePublicIp() {
            return acquirePublicIp;
        }

        public void setAcquirePublicIp(boolean acquirePublicIp) {
            this.acquirePublicIp = acquirePublicIp;
        }

        public int getPrivateIpsCount() {
            return privateIpsCount;
        }

        public void setPrivateIpsCount(int privateIpsCount) {
            this.privateIpsCount = privateIpsCount;
        }

        public Firewall getFirewall() {
            return firewall;
        }

        public void setFirewall(Firewall firewall) {
            this.firewall = firewall;
        }
    }





}
