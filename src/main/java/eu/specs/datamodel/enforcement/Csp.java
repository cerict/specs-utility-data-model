package eu.specs.datamodel.enforcement;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import eu.specs.datamodel.enforcement.ImplementationPlanTwoProviders.Iaas;
import eu.specs.datamodel.enforcement.ImplementationPlanTwoProviders.Pool;

public abstract class Csp {

    @JsonProperty("iaas")
    private Iaas iaas;
    @JsonProperty("pools")
    private List<Pool> pools = new ArrayList<Pool>();

    /**
     * 
     * @return
     *     The iaas
     */
    @JsonProperty("iaas")
    public Iaas getIaas() {
        return iaas;
    }

    /**
     * 
     * @param iaas
     *     The iaas
     */
    @JsonProperty("iaas")
    public void setIaas(Iaas iaas) {
        this.iaas = iaas;
    }

    /**
     * 
     * @return
     *     The pools
     */
    @JsonProperty("pools")
    public List<Pool> getPools() {
        return pools;
    }

    /**
     * 
     * @param pools
     *     The pools
     */
    @JsonProperty("pools")
    public void setPools(List<Pool> pools) {
        this.pools = pools;
    }
    
	public void addPool(Pool pool) {
		this.pools.add(pool);
	}


}