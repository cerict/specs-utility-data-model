package eu.specs.datamodel.enforcement;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

public class ChefRecipe implements Serializable {
    private static final long serialVersionUID = -8423207030032855910L;
    @JsonProperty("name")
    private String name;
    @JsonProperty("recipe_description")
    private String description;
    @JsonProperty("associated_metrics")
    private List<String> associatedMetrics;
    @JsonProperty("associated_measurements")
    private List<String> associatedMeasurements;
    @JsonProperty("dependent_components")
    private List<String> dependentComponents;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getAssociatedMetrics() {
        return associatedMetrics;
    }

    public void setAssociatedMetrics(List<String> associatedMetrics) {
        this.associatedMetrics = associatedMetrics;
    }

    public List<String> getAssociatedMeasurements() {
        return associatedMeasurements;
    }

    public void setAssociatedMeasurements(List<String> associatedMeasurements) {
        this.associatedMeasurements = associatedMeasurements;
    }

    public List<String> getDependentComponents() {
        return dependentComponents;
    }

    public void setDependentComponents(List<String> dependentComponents) {
        this.dependentComponents = dependentComponents;
    }

    @Override
    public String toString() {
        return name;
    }
}
