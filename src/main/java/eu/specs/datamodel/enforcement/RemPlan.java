package eu.specs.datamodel.enforcement;

import com.fasterxml.jackson.annotation.JsonProperty;
import eu.specs.datamodel.common.Annotation;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RemPlan {
    @JsonProperty("rem_plan_id")
    private String id;
    @JsonProperty("creation_time")
    private Date creationTime;
    @JsonProperty("sla_id")
    private String slaId;
    @JsonProperty("plan_id")
    private String planId;
    @JsonProperty("event_id")
    private String eventId;
    @JsonProperty("root_causes")
    private List<String> rootCauses = new ArrayList<>();
    @JsonProperty("remediation_flow")
    private RemFlow remFlow;
    @JsonProperty("remediation_actions")
    private List<RemAction> remActions = new ArrayList<>();
    @JsonProperty("chef_recipes")
    private List<ChefRecipe> chefRecipes = new ArrayList<>();
    @JsonProperty("result")
    private Result result;
    @JsonProperty("annotations")
    private List<Annotation> annotations = new ArrayList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    public String getSlaId() {
        return slaId;
    }

    public void setSlaId(String slaId) {
        this.slaId = slaId;
    }

    public String getPlanId() {
        return planId;
    }

    public void setPlanId(String planId) {
        this.planId = planId;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public List<String> getRootCauses() {
        return rootCauses;
    }

    public void setRootCauses(List<String> rootCauses) {
        this.rootCauses = rootCauses;
    }

    public void addRootCause(String rootCause) {
        this.rootCauses.add(rootCause);
    }

    public RemFlow getRemFlow() {
        return remFlow;
    }

    public void setRemFlow(RemFlow remFlow) {
        this.remFlow = remFlow;
    }

    public List<RemAction> getRemActions() {
        return remActions;
    }

    public void setRemActions(List<RemAction> remActions) {
        this.remActions = remActions;
    }

    public void addRemAction(RemAction remAction) {
        this.remActions.add(remAction);
    }

    public List<ChefRecipe> getChefRecipes() {
        return chefRecipes;
    }

    public void setChefRecipes(List<ChefRecipe> chefRecipes) {
        this.chefRecipes = chefRecipes;
    }

    public void addChefRecipe(ChefRecipe chefRecipe) {
        this.chefRecipes.add(chefRecipe);
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public List<Annotation> getAnnotations() {
        return annotations;
    }

    public void setAnnotations(List<Annotation> annotations) {
        this.annotations = annotations;
    }

    public void addAnnotation(Annotation annotation) {
        this.annotations.add(annotation);
    }

    public static enum Result {
        OBSERVE,
        NOTIFY,
        ERROR
    }
}
