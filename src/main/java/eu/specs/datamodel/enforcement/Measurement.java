package eu.specs.datamodel.enforcement;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

public class Measurement implements Serializable {

    private static final long serialVersionUID = -8449416339826910401L;
    @JsonProperty("msr_id")
    private String id;
    @JsonProperty("msr_description")
    private String description;
    @JsonProperty("frequency")
    private String frequency;
    @JsonProperty("metrics")
    private List<String> metrics;
    @JsonProperty("monitoring_event")
    private MonitoringEvent monitoringEvent;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public List<String> getMetrics() {
        return metrics;
    }

    public void setMetrics(List<String> metrics) {
        this.metrics = metrics;
    }

    public MonitoringEvent getMonitoringEvent() {
        return monitoringEvent;
    }

    public void setMonitoringEvent(MonitoringEvent monitoringEvent) {
        this.monitoringEvent = monitoringEvent;
    }

    public static class MonitoringEvent implements Serializable {

        private static final long serialVersionUID = 7903288796076053213L;
        @JsonProperty("event_id")
        private String id;
        @JsonProperty("event_description")
        private String description;
        @JsonProperty("event_type")
        private EventType eventType;
        @JsonProperty("condition")
        private Condition condition;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public EventType getEventType() {
            return eventType;
        }

        public void setEventType(EventType eventType) {
            this.eventType = eventType;
        }

        public Condition getCondition() {
            return condition;
        }

        public void setCondition(Condition condition) {
            this.condition = condition;
        }

        public static enum EventType implements Serializable {
            VIOLATION,
            ALERT
        }

        public static class Condition implements Serializable {

            private static final long serialVersionUID = 173705636116648507L;
            @JsonProperty("operator")
            private String operator;
            @JsonProperty("threshold")
            private String threshold;

            public String getOperator() {
                return operator;
            }

            public void setOperator(String operator) {
                this.operator = operator;
            }

            public String getThreshold() {
                return threshold;
            }

            public void setThreshold(String threshold) {
                this.threshold = threshold;
            }
        }
    }
}
