package eu.specs.datamodel.enforcement;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import eu.specs.datamodel.enforcement.ImplementationPlanTwoProviders.Component;

public class CspPaas extends Csp {

	public Paas paas;
	
	public static class Paas{
		@JsonProperty("components")
        private List<Component> components = new ArrayList<Component>();
		private String paasAgent;
		
		public List<Component> getComponents() {
			return components;
		}

		public void setComponents(List<Component> components) {
			this.components = components;
		}

		public String getPaasAgent (){
			return paasAgent;
		}

		public void setPaasAgent (String paasAgent){
			this.paasAgent = paasAgent;
		}

		@Override
		public String toString(){
			return "ClassPojo [components = "+components+", paasAgent = "+paasAgent+"]";
		}
	}
}
