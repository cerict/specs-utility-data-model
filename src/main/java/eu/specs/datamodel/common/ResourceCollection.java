package eu.specs.datamodel.common;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class ResourceCollection {
    @JsonProperty("resource")
    private String resource;
    @JsonProperty("total")
    private int total;
    @JsonProperty("members")
    private int members;
    @JsonProperty("item_list")
    private List<Item> itemList = new ArrayList<>();

    public String getResource() {
        return resource;
    }

    public void setResource(String resource) {
        this.resource = resource;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getMembers() {
        return members;
    }

    public void setMembers(int members) {
        this.members = members;
    }

    public List<Item> getItemList() {
        return itemList;
    }

    public void setItemList(List<Item> itemList) {
        this.itemList = itemList;
    }

    public void addItem(Item item) {
        this.itemList.add(item);
    }

    public static class Item {
        @JsonProperty("id")
        private String id;
        @JsonProperty("item")
        private String item;

        public Item() {
        }

        public Item(String id, String item) {
            this.id = id;
            this.item = item;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getItem() {
            return item;
        }

        public void setItem(String item) {
            this.item = item;
        }
    }
}
