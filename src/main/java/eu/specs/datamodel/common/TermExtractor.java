package eu.specs.datamodel.common;

import eu.specs.datamodel.agreement.terms.Term;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by adrian on 2/2/16.
 */
public class TermExtractor {
    private TermExtractor(){

    }

    public static List<Term> getTermsOfType(List<Term> termList, Class<?> c){
        List<Term> result = new ArrayList<Term>();
        for(Term t : termList){
            if (t.getClass() == c)
                result.add(t);
        }
        return result;
    }
}
