
package eu.specs.datamodel.sla.sdt;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for oneOpOperator.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="oneOpOperator">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="equal"/>
 *     &lt;enumeration value="greater then"/>
 *     &lt;enumeration value="less then"/>
 *     &lt;enumeration value="greater then or equal"/>
 *     &lt;enumeration value="less then or equal"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
//@XmlType(name = "oneOpOperator", namespace = "http://www.specs-project.eu/resources/schemas/xml/SLAtemplate")
@XmlEnum
public enum OneOpOperator {

    @XmlEnumValue("eq")
    EQUAL("eq"),
    @XmlEnumValue("neq")
    NOT_EQUAL("neq"),
    @XmlEnumValue("gt")
    GREATER_THAN("gt"),
    @XmlEnumValue("lt")
    LESS_THAN("lt"),
    @XmlEnumValue("geq")
    GREATER_THAN_OR_EQUAL("geq"),
    @XmlEnumValue("leq")
    LESS_THAN_OR_EQUAL("leq");
    private final String value;

    OneOpOperator(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static OneOpOperator fromValue(String v) {
        for (OneOpOperator c: OneOpOperator.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

    public OneOpOperator getInverse() {
        switch (this) {
            case EQUAL:
                return NOT_EQUAL;
            case NOT_EQUAL:
                return EQUAL;
            case GREATER_THAN:
                return LESS_THAN_OR_EQUAL;
            case LESS_THAN:
                return GREATER_THAN_OR_EQUAL;
            case GREATER_THAN_OR_EQUAL:
                return LESS_THAN;
            case LESS_THAN_OR_EQUAL:
                return GREATER_THAN;
            default:
                throw new UnsupportedOperationException(String.format("The operator %s is not supported.", this));
        }
    }
}
