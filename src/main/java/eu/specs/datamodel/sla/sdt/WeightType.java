
package eu.specs.datamodel.sla.sdt;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for weightType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="weightType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="LOW"/>
 *     &lt;enumeration value="MEDIUM"/>
 *     &lt;enumeration value="HIGH"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "weightType", namespace = "http://www.specs-project.eu/resources/schemas/xml/SLAtemplate")
@XmlEnum
public enum WeightType {

    LOW,
    MEDIUM,
    HIGH;

    public String value() {
        return name();
    }

    public static WeightType fromValue(String v) {
        return valueOf(v);
    }

}
